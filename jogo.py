from re import M
from time import sleep
import tkinter
import pygame as pg
from pygame.locals import *
import sys
from math import floor
from tkinter import *

# Iniciando o pygame
pg.init()

# Algumas variaveis
posicoes_escolhidas = []
vez_x_jogar = True
pos_x = 0
pos_y = 0

# Janela
largura = 900
altura = 500

janela = pg.display.set_mode((largura, altura))
pg.display.set_caption('Jogo da Velha')

pg.font.init()
font = pg.font.Font(None, 50)
x_vencedor = font.render("X é o vencedor!", True, (255, 255, 255))
o_vencedor = font.render("O é o vencedor!", True, (255, 255, 255))

def tabuleiro(janela):
    # Linhas na vertical
    pg.draw.line(janela, (255, 255, 255), (360, 50), (360, 450), 5)
    pg.draw.line(janela, (255, 255, 255), (540, 50), (540, 450), 5)

    # Linhas na horizontal
    pg.draw.line(janela, (255, 255, 255), (200, 160), (700, 160), 5)
    pg.draw.line(janela, (255, 255, 255), (200, 320), (700, 320), 5)

def vencedor():
    # Se o X ganhar
    fim_jogo = 0
    if '1, 0, X' in posicoes_escolhidas and '2, 0, X' in posicoes_escolhidas and '3, 0, X' in posicoes_escolhidas:
        pg.draw.line(janela, (0, 255, 0), (200, 115), (700, 115), 5)
        janela.blit(x_vencedor, (325, 10))
    elif '1, 1, X' in posicoes_escolhidas and '2, 1, X' in posicoes_escolhidas and '3, 1, X' in posicoes_escolhidas:
        pg.draw.line(janela, (0, 255, 0), (200, 255), (700, 255), 5)
        janela.blit(x_vencedor, (325, 10))
    elif '1, 2, X' in posicoes_escolhidas and '2, 2, X' in posicoes_escolhidas and '3, 2, X' in posicoes_escolhidas:
        pg.draw.line(janela, (0, 255, 0), (200, 400), (700, 400), 5)
        janela.blit(x_vencedor, (325, 10))
    elif '1, 0, X' in posicoes_escolhidas and '2, 1, X' in posicoes_escolhidas and '3, 2, X' in posicoes_escolhidas:
        pg.draw.line(janela, (0, 255, 0), (200, 60), (700, 455), 5)
        janela.blit(x_vencedor, (325, 10))
    elif '3, 0, X' in posicoes_escolhidas and '2, 1, X' in posicoes_escolhidas and '1, 2, X' in posicoes_escolhidas:
        pg.draw.line(janela, (0, 255, 0), (200, 455), (700, 60), 5)
        janela.blit(x_vencedor, (325, 10))
    elif '1, 0, X' in posicoes_escolhidas and '1, 1, X' in posicoes_escolhidas and '1, 2, X' in posicoes_escolhidas:
        pg.draw.line(janela, (0, 255, 0), (275, 50), (275, 450), 5)
        janela.blit(x_vencedor, (325, 10))
    elif '2, 0, X' in posicoes_escolhidas and '2, 1, X' in posicoes_escolhidas and '2, 2, X' in posicoes_escolhidas:
        pg.draw.line(janela, (0, 255, 0), (455, 50), (455, 450), 5)
        janela.blit(x_vencedor, (325, 10))
    elif '3, 0, X' in posicoes_escolhidas and '3, 1, X' in posicoes_escolhidas and '3, 2, X' in posicoes_escolhidas:
        pg.draw.line(janela, (0, 255, 0), (627, 50), (627, 450), 5)
        janela.blit(x_vencedor, (325, 10))

    # Se a O ganhar
    if '1, 0, O' in posicoes_escolhidas and '2, 0, O' in posicoes_escolhidas and '3, 0, O' in posicoes_escolhidas:
        pg.draw.line(janela, (0, 255, 0), (200, 115), (700, 115), 5)
        janela.blit(o_vencedor, (325, 10))
    elif '1, 1, O' in posicoes_escolhidas and '2, 1, O' in posicoes_escolhidas and '3, 1, O' in posicoes_escolhidas:
        pg.draw.line(janela, (0, 255, 0), (200, 255), (700, 255), 5)
        janela.blit(o_vencedor, (325, 10))
    elif '1, 2, O' in posicoes_escolhidas and '2, 2, O' in posicoes_escolhidas and '3, 2, O' in posicoes_escolhidas:
        pg.draw.line(janela, (0, 255, 0), (200, 400), (700, 400), 5)
        janela.blit(o_vencedor, (325, 10))
    elif '1, 0, O' in posicoes_escolhidas and '2, 1, O' in posicoes_escolhidas and '3, 2, O' in posicoes_escolhidas:
        pg.draw.line(janela, (0, 255, 0), (200, 60), (700, 455), 5)
        janela.blit(o_vencedor, (325, 10))
    elif '3, 0, O' in posicoes_escolhidas and '2, 1, O' in posicoes_escolhidas and '1, 2, O' in posicoes_escolhidas:
        pg.draw.line(janela, (0, 255, 0), (200, 455), (700, 60), 5)
        janela.blit(o_vencedor, (325, 10))
    elif '1, 0, O' in posicoes_escolhidas and '1, 1, O' in posicoes_escolhidas and '1, 2, O' in posicoes_escolhidas:
        pg.draw.line(janela, (0, 255, 0), (275, 50), (275, 450), 5)
        janela.blit(o_vencedor, (325, 10))
    elif '2, 0, O' in posicoes_escolhidas and '2, 1, O' in posicoes_escolhidas and '2, 2, O' in posicoes_escolhidas:
        pg.draw.line(janela, (0, 255, 0), (455, 50), (455, 450), 5)
        janela.blit(o_vencedor, (325, 10))
    elif '3, 0, O' in posicoes_escolhidas and '3, 1, O' in posicoes_escolhidas and '3, 2, O' in posicoes_escolhidas:
        pg.draw.line(janela, (0, 255, 0), (627, 50), (627, 450), 5)
        janela.blit(o_vencedor, (325, 10))

    return fim_jogo

def validaInsereXO():
    resposta = resultado_equacao.get()

    if pergunta == '2X = 4':
        if resposta == '2':
            if vez_x_jogar == True:
                print('É IGUAL A DOIS!')
                posicoes_escolhidas.append('1, 0, X')
                janela.blit(x, (265, 104))
            else:
                print('É IGUAL A DOIS!')
                posicoes_escolhidas.append('1, 0, O')
                janela.blit(o, (265, 104)) 
    elif pergunta == '4X = 16':
        if resposta == '4':
            if vez_x_jogar == True:
                posicoes_escolhidas.append('2, 0, X')
                janela.blit(x, (444, 100))
            else:
                posicoes_escolhidas.append('2, 0, O')
                janela.blit(o, (444, 100))
    elif pergunta == '6X = 36':
        if resposta == '6':
            if vez_x_jogar == True:
                posicoes_escolhidas.append('3, 0, X')
                janela.blit(x, (616, 101))
            else:
                posicoes_escolhidas.append('3, 0, O')
                janela.blit(o, (616, 101))
    elif pergunta == '7X = 49':
        if resposta == '7':
            if vez_x_jogar == True:
                posicoes_escolhidas.append('1, 1, X')
                janela.blit(x, (269, 238))
            else:
                janela.blit(o, (269, 238))
                posicoes_escolhidas.append('1, 1, O')
    elif pergunta == '4 = b - 5':
        if resposta == '9':
            if vez_x_jogar == True:
                janela.blit(x, (263, 385))
                posicoes_escolhidas.append('1, 2, X')
            else:
                janela.blit(o, (263, 385))
                posicoes_escolhidas.append('1, 2, O')
    elif pergunta == 'y - 2 = -12':
        if resposta == '-10':
            if vez_x_jogar == True:
                janela.blit(x, (443, 243))
                posicoes_escolhidas.append('2, 1, X')
            else:
                janela.blit(o, (443, 243))
                posicoes_escolhidas.append('2, 1, O')
    elif pergunta == 'x + 1 = 15':
        if resposta == '14':
            if vez_x_jogar == True:
                janela.blit(x, (617, 240))
                posicoes_escolhidas.append('3, 1, X')
            else:
                janela.blit(o, (617, 240))
                posicoes_escolhidas.append('3, 1, O')
    elif pergunta == 'z - 3 = -9':
        if resposta == '-6':
            if vez_x_jogar == True:
                janela.blit(x, (440, 384))
                posicoes_escolhidas.append('2, 2, X')
            else:
                janela.blit(o, (440, 384))
                posicoes_escolhidas.append('2, 2, O')
    elif pergunta == '3 + 2x = 6 - x':
        if resposta == '1':
            if vez_x_jogar == True:
                janela.blit(x, (615, 386))
                posicoes_escolhidas.append('3, 2, X')
            else:
                janela.blit(o, (615, 386))
                posicoes_escolhidas.append('3, 2, O')

# roda o loop do jogo
while True:
    for event in pg.event.get():
        if event.type == QUIT:
            pg.quit()
            sys.exit()

    tabuleiro(janela)

    if event.type == pg.MOUSEBUTTONUP:
        if vez_x_jogar == True:
            pos_x_clicou = pg.mouse.get_pos()
            pos_x = floor(pos_x_clicou[0]/180)
            pos_y = floor(pos_x_clicou[1]/160)
            simbolo = 'X'
        else:
            pos_o_clicou = pg.mouse.get_pos()
            pos_x = floor(pos_o_clicou[0]/180)
            pos_y = floor(pos_o_clicou[1]/160)
            simbolo = 'O'


        posicao_escolhida = (pos_x, pos_y, simbolo)
        print(posicao_escolhida)

        global x
        x = font.render("X", True, (255, 255, 255))
        global o
        o = font.render("O", True, (255, 255, 255))

        if vez_x_jogar == True and str(posicao_escolhida) == "(1, 0, 'X')":
            if "1, 0, O" in posicoes_escolhidas:
                pass
            else:
                if "1, 0, X" not in posicoes_escolhidas:
                    master = Tk()
                    master.title("Digite o resultado")
                    master.geometry("400x200")
                    master.wm_resizable(width=False, height=False)

                    pergunta = '2X = 4'
                    texto = Label(master, text=pergunta)
                    texto['font'] = ('Calibri', '20')
                    texto.pack()

                    resultado_equacao = Entry(master, bd=2, font=('Calibri', 15), justify=CENTER)
                    resultado_equacao.place(width=300, height=45, x=49, y=80)

                    botao = Button(master, text='Enviar', command=validaInsereXO)
                    botao.place(width=90, height=40, x=150, y=150)

                    master.mainloop()
                    vez_x_jogar = False

                else:
                    pass

        elif vez_x_jogar == True and str(posicao_escolhida) == "(2, 0, 'X')":
            if "2, 0, O" in posicoes_escolhidas:
                pass
            else:
                if "2, 0, O" not in posicoes_escolhidas:
                    master = Tk()
                    master.title("Digite o resultado")
                    master.geometry("400x200")
                    master.wm_resizable(width=False, height=False)

                    pergunta = '4X = 16'
                    print(pergunta)
                    texto = Label(master, text=pergunta)
                    texto['font'] = ('Calibri', '20')
                    texto.pack()

                    resultado_equacao = Entry(master, bd=2, font=('Calibri', 15), justify=CENTER)
                    resultado_equacao.place(width=300, height=45, x=49, y=80)

                    botao = Button(master, text='Enviar', command=validaInsereXO)
                    botao.place(width=90, height=40, x=150, y=150)

                    master.mainloop()
                    vez_x_jogar = False
                else:
                    pass
        
        elif vez_x_jogar == True and str(posicao_escolhida) == "(3, 0, 'X')":
            if "3, 0, O" in posicoes_escolhidas:
                pass
            else:
                if "3, 0, X" not in posicoes_escolhidas:
                    master = Tk()
                    master.title("Digite o resultado")
                    master.geometry("400x200")
                    master.wm_resizable(width=False, height=False)

                    pergunta = '6X = 36'
                    texto = Label(master, text=pergunta)
                    texto['font'] = ('Calibri', '20')
                    texto.pack()

                    resultado_equacao = Entry(master, bd=2, font=('Calibri', 15), justify=CENTER)
                    resultado_equacao.place(width=300, height=45, x=49, y=80)

                    botao = Button(master, text='Enviar', command=validaInsereXO)
                    botao.place(width=90, height=40, x=150, y=150)

                    master.mainloop()
                    vez_x_jogar = False
                else:
                    pass

        elif vez_x_jogar == True and str(posicao_escolhida) == "(1, 1, 'X')":
            if "1, 1, O" in posicoes_escolhidas:
                pass
            else:
                if "1, 1, X" not in posicoes_escolhidas:
                    master = Tk()
                    master.title("Digite o resultado")
                    master.geometry("400x200")
                    master.wm_resizable(width=False, height=False)

                    pergunta = '7X = 49'
                    texto = Label(master, text=pergunta)
                    texto['font'] = ('Calibri', '20')
                    texto.pack()

                    resultado_equacao = Entry(master, bd=2, font=('Calibri', 15), justify=CENTER)
                    resultado_equacao.place(width=300, height=45, x=49, y=80)

                    botao = Button(master, text='Enviar', command=validaInsereXO)
                    botao.place(width=90, height=40, x=150, y=150)

                    master.mainloop()
                    vez_x_jogar = False
                else:
                    pass

        elif vez_x_jogar == True and str(posicao_escolhida) == "(1, 2, 'X')":
            if "1, 2, O" in posicoes_escolhidas:
                pass
            else:
                if "1, 2, X" not in posicoes_escolhidas:
                    master = Tk()
                    master.title("Digite o resultado")
                    master.geometry("400x200")
                    master.wm_resizable(width=False, height=False)

                    pergunta = '4 = b - 5'
                    texto = Label(master, text=pergunta)
                    texto['font'] = ('Calibri', '20')
                    texto.pack()

                    resultado_equacao = Entry(master, bd=2, font=('Calibri', 15), justify=CENTER)
                    resultado_equacao.place(width=300, height=45, x=49, y=80)

                    botao = Button(master, text='Enviar', command=validaInsereXO)
                    botao.place(width=90, height=40, x=150, y=150)

                    master.mainloop()
                    vez_x_jogar = False
                else:
                    pass
        
        elif vez_x_jogar == True and str(posicao_escolhida) == "(2, 1, 'X')":
            if "2, 1, O" in posicoes_escolhidas:
                pass
            else:
                if "2, 1, X" not in posicoes_escolhidas:
                    master = Tk()
                    master.title("Digite o resultado")
                    master.geometry("400x200")
                    master.wm_resizable(width=False, height=False)

                    pergunta = 'y - 2 = -12'
                    texto = Label(master, text=pergunta)
                    texto['font'] = ('Calibri', '20')
                    texto.pack()

                    resultado_equacao = Entry(master, bd=2, font=('Calibri', 15), justify=CENTER)
                    resultado_equacao.place(width=300, height=45, x=49, y=80)

                    botao = Button(master, text='Enviar', command=validaInsereXO)
                    botao.place(width=90, height=40, x=150, y=150)

                    master.mainloop()
                    vez_x_jogar = False
                else:
                    pass
        
        elif vez_x_jogar == True and str(posicao_escolhida) == "(3, 1, 'X')":
            if "3, 1, O" in posicoes_escolhidas:
                pass
            else:
                if "3, 1, X" not in posicoes_escolhidas:
                    master = Tk()
                    master.title("Digite o resultado")
                    master.geometry("400x200")
                    master.wm_resizable(width=False, height=False)

                    pergunta = 'x + 1 = 15'
                    texto = Label(master, text=pergunta)
                    texto['font'] = ('Calibri', '20')
                    texto.pack()

                    resultado_equacao = Entry(master, bd=2, font=('Calibri', 15), justify=CENTER)
                    resultado_equacao.place(width=300, height=45, x=49, y=80)

                    botao = Button(master, text='Enviar', command=validaInsereXO)
                    botao.place(width=90, height=40, x=150, y=150)

                    master.mainloop()
                    vez_x_jogar = False
                else:
                    pass
        
        elif vez_x_jogar == True and str(posicao_escolhida) == "(2, 2, 'X')":
            if "2, 2, O" in posicoes_escolhidas:
                pass
            else:
                if "2, 2, X" not in posicoes_escolhidas:
                    master = Tk()
                    master.title("Digite o resultado")
                    master.geometry("400x200")
                    master.wm_resizable(width=False, height=False)

                    pergunta = 'z - 3 = -9'
                    texto = Label(master, text=pergunta)
                    texto['font'] = ('Calibri', '20')
                    texto.pack()

                    resultado_equacao = Entry(master, bd=2, font=('Calibri', 15), justify=CENTER)
                    resultado_equacao.place(width=300, height=45, x=49, y=80)

                    botao = Button(master, text='Enviar', command=validaInsereXO)
                    botao.place(width=90, height=40, x=150, y=150)

                    master.mainloop()
                    vez_x_jogar = False
                else:
                    pass
        
        elif vez_x_jogar == True and str(posicao_escolhida) == "(3, 2, 'X')":
            if "3, 2, O" in posicoes_escolhidas:
                pass
            else:
                if "3, 2, X" not in posicoes_escolhidas:
                    master = Tk()
                    master.title("Digite o resultado")
                    master.geometry("400x200")
                    master.wm_resizable(width=False, height=False)

                    pergunta = '3 + 2x = 6 - x'
                    texto = Label(master, text=pergunta)
                    texto['font'] = ('Calibri', '20')
                    texto.pack()

                    resultado_equacao = Entry(master, bd=2, font=('Calibri', 15), justify=CENTER)
                    resultado_equacao.place(width=300, height=45, x=49, y=80)

                    botao = Button(master, text='Enviar', command=validaInsereXO)
                    botao.place(width=90, height=40, x=150, y=150)

                    master.mainloop()
                    vez_x_jogar = False
                else:
                    pass

        ## Para a bolinha
        if vez_x_jogar == False and str(posicao_escolhida) == "(1, 0, 'O')":
            if '1, 0, X' in posicoes_escolhidas:
                pass
            else:
                if "1, 0, O" not in posicoes_escolhidas:
                    master = Tk()
                    master.title("Digite o resultado")
                    master.geometry("400x200")
                    master.wm_resizable(width=False, height=False)

                    pergunta = '2X = 4'
                    texto = Label(master, text=pergunta)
                    texto['font'] = ('Calibri', '20')
                    texto.pack()

                    resultado_equacao = Entry(master, bd=2, font=('Calibri', 15), justify=CENTER)
                    resultado_equacao.place(width=300, height=45, x=49, y=80)

                    botao = Button(master, text='Enviar', command=validaInsereXO)
                    botao.place(width=90, height=40, x=150, y=150)

                    master.mainloop()
                    vez_x_jogar = True
                else:
                    pass

        elif vez_x_jogar == False and str(posicao_escolhida) == "(2, 0, 'O')":
            if "2, 0, X" in posicoes_escolhidas:
                pass
            else:
                if "2, 0, O" not in posicoes_escolhidas:
                    master = Tk()
                    master.title("Digite o resultado")
                    master.geometry("400x200")
                    master.wm_resizable(width=False, height=False)

                    pergunta = '4X = 16'
                    print(pergunta)
                    texto = Label(master, text=pergunta)
                    texto['font'] = ('Calibri', '20')
                    texto.pack()

                    resultado_equacao = Entry(master, bd=2, font=('Calibri', 15), justify=CENTER)
                    resultado_equacao.place(width=300, height=45, x=49, y=80)

                    botao = Button(master, text='Enviar', command=validaInsereXO)
                    botao.place(width=90, height=40, x=150, y=150)

                    master.mainloop()
                    vez_x_jogar = True
                else:
                    pass
        
        elif vez_x_jogar == False and str(posicao_escolhida) == "(3, 0, 'O')":
            if "3, 0, X" in posicoes_escolhidas:
                pass
            else:
                if "3, 0, O" not in posicoes_escolhidas:
                    master = Tk()
                    master.title("Digite o resultado")
                    master.geometry("400x200")
                    master.wm_resizable(width=False, height=False)

                    pergunta = '6X = 36'
                    print(pergunta)
                    texto = Label(master, text=pergunta)
                    texto['font'] = ('Calibri', '20')
                    texto.pack()

                    resultado_equacao = Entry(master, bd=2, font=('Calibri', 15), justify=CENTER)
                    resultado_equacao.place(width=300, height=45, x=49, y=80)

                    botao = Button(master, text='Enviar', command=validaInsereXO)
                    botao.place(width=90, height=40, x=150, y=150)

                    master.mainloop()
                    vez_x_jogar = True
                else:
                    pass

        elif vez_x_jogar == False and str(posicao_escolhida) == "(1, 1, 'O')":
            if "1, 1, X" in posicoes_escolhidas:
                pass
            else:
                if "1, 1, O" not in posicoes_escolhidas:
                    master = Tk()
                    master.title("Digite o resultado")
                    master.geometry("400x200")
                    master.wm_resizable(width=False, height=False)

                    pergunta = '7X = 49'
                    print(pergunta)
                    texto = Label(master, text=pergunta)
                    texto['font'] = ('Calibri', '20')
                    texto.pack()

                    resultado_equacao = Entry(master, bd=2, font=('Calibri', 15), justify=CENTER)
                    resultado_equacao.place(width=300, height=45, x=49, y=80)

                    botao = Button(master, text='Enviar', command=validaInsereXO)
                    botao.place(width=90, height=40, x=150, y=150)

                    master.mainloop()
                    vez_x_jogar = True
                else:
                    pass

        elif vez_x_jogar == False and str(posicao_escolhida) == "(1, 2, 'O')":
            if "1, 2, X" in posicoes_escolhidas:
                pass
            else:
                if "1, 2, O" not in posicoes_escolhidas:
                    master = Tk()
                    master.title("Digite o resultado")
                    master.geometry("400x200")
                    master.wm_resizable(width=False, height=False)

                    pergunta = '4 = b - 5'
                    print(pergunta)
                    texto = Label(master, text=pergunta)
                    texto['font'] = ('Calibri', '20')
                    texto.pack()

                    resultado_equacao = Entry(master, bd=2, font=('Calibri', 15), justify=CENTER)
                    resultado_equacao.place(width=300, height=45, x=49, y=80)

                    botao = Button(master, text='Enviar', command=validaInsereXO)
                    botao.place(width=90, height=40, x=150, y=150)

                    master.mainloop()
                    vez_x_jogar = True
                else:
                    pass
        
        elif vez_x_jogar == False and str(posicao_escolhida) == "(2, 1, 'O')":
            if "2, 1, X" in posicoes_escolhidas:
                pass
            else:
                if "2, 1, O" not in posicoes_escolhidas:
                    master = Tk()
                    master.title("Digite o resultado")
                    master.geometry("400x200")
                    master.wm_resizable(width=False, height=False)

                    pergunta = 'y - 2 = -12'
                    print(pergunta)
                    texto = Label(master, text=pergunta)
                    texto['font'] = ('Calibri', '20')
                    texto.pack()

                    resultado_equacao = Entry(master, bd=2, font=('Calibri', 15), justify=CENTER)
                    resultado_equacao.place(width=300, height=45, x=49, y=80)

                    botao = Button(master, text='Enviar', command=validaInsereXO)
                    botao.place(width=90, height=40, x=150, y=150)

                    master.mainloop()
                    vez_x_jogar = True
                else:
                    pass
        
        elif vez_x_jogar == False and str(posicao_escolhida) == "(3, 1, 'O')":
            if "3, 1, X" in posicoes_escolhidas:
                pass
            else:
                if "3, 1, O" not in posicoes_escolhidas:
                    master = Tk()
                    master.title("Digite o resultado")
                    master.geometry("400x200")
                    master.wm_resizable(width=False, height=False)

                    pergunta = 'x + 1 = 15'
                    print(pergunta)
                    texto = Label(master, text=pergunta)
                    texto['font'] = ('Calibri', '20')
                    texto.pack()

                    resultado_equacao = Entry(master, bd=2, font=('Calibri', 15), justify=CENTER)
                    resultado_equacao.place(width=300, height=45, x=49, y=80)

                    botao = Button(master, text='Enviar', command=validaInsereXO)
                    botao.place(width=90, height=40, x=150, y=150)

                    master.mainloop()
                    vez_x_jogar = True
                else:
                    pass
        
        elif vez_x_jogar == False and str(posicao_escolhida) == "(2, 2, 'O')":
            if "2, 2, X" in posicoes_escolhidas:
                pass
            else:
                if "2, 2, O" not in posicoes_escolhidas:
                    master = Tk()
                    master.title("Digite o resultado")
                    master.geometry("400x200")
                    master.wm_resizable(width=False, height=False)

                    pergunta = 'z - 3 = -9'
                    print(pergunta)
                    texto = Label(master, text=pergunta)
                    texto['font'] = ('Calibri', '20')
                    texto.pack()

                    resultado_equacao = Entry(master, bd=2, font=('Calibri', 15), justify=CENTER)
                    resultado_equacao.place(width=300, height=45, x=49, y=80)

                    botao = Button(master, text='Enviar', command=validaInsereXO)
                    botao.place(width=90, height=40, x=150, y=150)

                    master.mainloop()
                    vez_x_jogar = True
                else:
                    pass
        
        elif vez_x_jogar == False and str(posicao_escolhida) == "(3, 2, 'O')":
            if "3, 2, X" in posicoes_escolhidas:
                pass
            else:
                if "3, 2, O" not in posicoes_escolhidas:
                    master = Tk()
                    master.title("Digite o resultado")
                    master.geometry("400x200")
                    master.wm_resizable(width=False, height=False)

                    pergunta = '3 + 2x = 6 - x'
                    print(pergunta)
                    texto = Label(master, text=pergunta)
                    texto['font'] = ('Calibri', '20')
                    texto.pack()

                    resultado_equacao = Entry(master, bd=2, font=('Calibri', 15), justify=CENTER)
                    resultado_equacao.place(width=300, height=45, x=49, y=80)

                    botao = Button(master, text='Enviar', command=validaInsereXO)
                    botao.place(width=90, height=40, x=150, y=150)

                    master.mainloop()
                    vez_x_jogar = True
                else:
                    pass

    teste = vencedor()

    pg.display.update()